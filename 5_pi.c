#include <omp.h>
#include <stdio.h>

int main() {
    int num_steps = 1000000000;
    double step = 1 / (double)num_steps;

    int req_threads = omp_get_max_threads();
    printf("Asking for %d threads\n", req_threads);

    double start = omp_get_wtime();

    double sum = 0.0;
    #pragma omp parallel for reduction(+ : sum)
    for (int i = 0; i < num_steps; i++) {
        double x = (i + 0.5) * step;
        sum += 4.0 / (1.0 + x * x);
    }

    double pi = sum * step;
    double end = omp_get_wtime();

    printf("Calculated pi in %f seconds: %f\n", end - start, pi);
}