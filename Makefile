CFLAGS = -Wall -Wpedantic -std=c99
LINKER_FLAGS = -fopenmp

hello_world:
	$(CC) 2_hello_world.c $(CFLAGS) $(LINKER_FLAGS) -o bin/hello_world
	bin/hello_world

pi_false_sharing:
	$(CC) 3_pi.c $(CFLAGS) $(LINKER_FLAGS) -o bin/pi_false_sharing
	bin/pi_false_sharing

pi_atomic:
	$(CC) 4_pi.c $(CFLAGS) $(LINKER_FLAGS) -o bin/pi_atomic
	bin/pi_atomic

pi_reduction:
	$(CC) 5_pi.c $(CFLAGS) $(LINKER_FLAGS) -o bin/pi_reduction
	bin/pi_reduction

mandelbrot:
	$(CC) 6_mandelbrot.c $(CFLAGS) $(LINKER_FLAGS) -o bin/mandelbrot
	bin/mandelbrot