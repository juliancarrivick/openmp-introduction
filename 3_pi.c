#include <omp.h>
#include <stdio.h>

#define PAD 8

int main() {
    int num_steps = 100000000;
    double step = 1 / (double)num_steps;

    double start = omp_get_wtime();

    int req_threads = omp_get_max_threads();
    printf("Asking for %d threads\n", req_threads);

    // Add padding to size of 64 byte cache line to
    // prevent false-sharing between threads
    double sums[req_threads][PAD];
    int act_threads = 0;
    #pragma omp parallel
    {
        int id = omp_get_thread_num();

        // We only know the number of threads we are running
        // with once we enter the parallel section
        int nthreads = omp_get_num_threads();
        if (id == 0) {
            act_threads = nthreads;
            printf("We got %d threads\n", act_threads);
        }

        sums[id][0] = 0.0;
        for (int i = id; i < num_steps; i += nthreads) {
            double x = (i + 0.5) * step;
            sums[id][0] += 4.0 / (1.0 + x * x);
        }
    }

    double sum = 0.0;
    for (int i = 0; i < act_threads; i++) {
        sum += sums[i][0];
    }

    double pi = sum * step;
    double end = omp_get_wtime();

    printf("Calculated pi in %f seconds: %f\n", end-start, pi);
}