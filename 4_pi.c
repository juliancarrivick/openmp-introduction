#include <omp.h>
#include <stdio.h>

int main() {
    int num_steps = 100000000;
    double step = 1 / (double)num_steps;

    double start = omp_get_wtime();

    int req_threads = omp_get_max_threads();
    printf("Asking for %d threads\n", req_threads);

    double sum = 0.0;
    #pragma omp parallel
    {
        int id = omp_get_thread_num();

        // We only know the number of threads we are running
        // with once we enter the parallel section
        int nthreads = omp_get_num_threads();
        if (id == 0) {
            printf("We got %d threads\n", nthreads);
        }

        double local_sum = 0.0;
        for (int i = id; i < num_steps; i += nthreads) {
            double x = (i + 0.5) * step;
            local_sum += 4.0 / (1.0 + x * x);
        }

        #pragma omp atomic
            sum += local_sum;
    }

    double pi = sum * step;
    double end = omp_get_wtime();

    printf("Calculated pi in %f seconds: %f\n", end-start, pi);
}